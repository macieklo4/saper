import numpy as np
import random


def choose_level():
    level = input('Type level... ')
    while level.lower() != 'easy' and level.lower() != 'medium' and level.lower() != 'hard':
        level = input('Type level... ')
    if level.lower() == 'easy':
        size = 5
        mines = 5
    elif level.lower() == 'medium':
        size = 8
        mines = 6
    elif level.lower() == 'hard':
        size = 10
        mines = 20
    return size, mines


def choose_coords(size, empty_board):
    while True:
        try:
            row = int(input(f'Enter row from 1 to {size}... '))
            column = int(input(f'Enter column from 1 to {size}... '))
            break
        except ValueError:
            print('Try Again')
    while not (row > 0 and row < (size+1) and column > 0 and column < (size+1) and empty_board[row, column] == -1):
        print('Invalid data')
        row = int(input(f'Enter row from 1 to {size}... '))
        column = int(input(f'Enter column from 1 to {size}... '))
    return row, column


def empty_table(size):
    empty_table = np.zeros((size+2, size+2))
    for i in range(size+2):
        for j in range(size+2):
            empty_table[i, j] = -1
    return empty_table


def mine_table(size, mines, row, column):
    size_plus = size+2
    mine_table = np.zeros((size_plus, size_plus))
    j = 0
    while j < mines:
        ran_1 = random.randint(1, size)
        ran_2 = random.randint(1, size)
        if mine_table[ran_1, ran_2] == 0 and ran_1 != row and ran_2 != column:
            mine_table[ran_1, ran_2] = 9
            j += 1
    return mine_table


def neighbour_block(mine_table, empty_table, v, i):
    lenght = len(empty_table)
    if empty_table[v, i] == -1 and v != 0 and i != 0 and v != (lenght-1) and i != (lenght-1):
        value = int((mine_table[v-1, i-1]+mine_table[v+1, i+1]+mine_table[v, i-1]+mine_table[v+1,i-1]+mine_table[v-1, i]+mine_table[v+1, i]+mine_table[v-1, i+1]+mine_table[v, i+1])/9)
        empty_table[v, i] = value
        if value == 0:
            neighbour_block(mine_table, empty_table, v-1, i-1)
            neighbour_block(mine_table, empty_table, v+1, i+1)
            neighbour_block(mine_table, empty_table, v, i-1)
            neighbour_block(mine_table, empty_table, v+1, i-1)
            neighbour_block(mine_table, empty_table, v-1, i)
            neighbour_block(mine_table, empty_table, v+1, i)
            neighbour_block(mine_table, empty_table, v-1, i+1)
            neighbour_block(mine_table, empty_table, v, i+1)


def print_view(empty_table):
    q = ''
    lenght = len(empty_table)
    for i in range(1, lenght-1):
        for j in range(1, lenght-1):
            if empty_table[i, j] == -1:
                q += '_ '
            elif empty_table[i, j] == 9:
                q += 'x '
            else:
                q += f'{int(empty_table[i,j])} '
        q += '\n'
    print(q)


def game():
    size, mines = choose_level()
    empty_board = empty_table(size)
    while True:
        end = 0
        row, column = choose_coords(size, empty_board)
        if not 'mine_board' in locals():
            mine_board = mine_table(size, mines, row, column)
        if mine_board[row, column] != 9:
            neighbour_block(mine_board, empty_board, row, column)
        else:
            empty_board[row, column] = 9
            print_view(empty_board)
            print('YOU LOST')
            break
        for k in range(1, size+1):
            for g in range(1, size+1):
                if empty_board[k, g] == -1:
                    end += 1
        if mines == end:
            for i in range(1, size+1):
                for j in range(1, size+1):
                    if empty_board[i, j] == -1:
                        empty_board[i, j] = 9
            print_view(empty_board)
            print('YOU WON')
            break
        else:
            print_view(empty_board)


if '__main__' == __name__:
    game()
